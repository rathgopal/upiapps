package com.upi.installedapp.plugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.content.ComponentName;
import android.os.Bundle;
import java.util.Iterator;
import java.util.Set;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;

import android.util.Base64;
import android.util.Base64OutputStream;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Canvas;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.LayerDrawable;

import org.apache.cordova.PluginResult;


public class AndroidInstalledApps extends CordovaPlugin {

    private final String[] upiAppPkgs = {"com.google.android.apps.nbu.paisa.user",
        "net.one97.paytm",  "com.phonepe.app", "com.csam.icici.bank.imobile", "com.freecharge.android",
        "com.whatsapp", "com.mobikwik_new", "com.truecaller", "com.bsb.hike", "com.myairtelapp","com.protinus.trupay",
        "com.yapapp.cu", "com.icicibank.pockets", "com.khaalijeb.inkdrops","com.evokio.finmo",
        "com.mycompany.kvb", "com.icici.eazypaycollect", "com.ultracash.payment.customer",
        "com.mgs.obcbank", "com.YesBank", "com.mgs.induspsp","com.ifThenElse.app.pay",
        "in.org.npci.upiapp", "com.sbi.upi", "com.upi.axispay","com.bankofbaroda.upi",
        "com.fss.unbipsp", "com.fss.idfcpsp", "com.fss.vijayapsp", "com.infrasofttech.mahaupi",
        "com.sc.scbupi", "com.infrasofttech.centralbankupi", "upi.npst.com.upicanara",
        "com.lcode.smartz", "com.fss.pnbpsp", "com.upi.federalbank.org.lotza",
        "com.olive.kotak.upi", "com.fisglobal.syndicateupi.app", "com.abipbl.upi",
        "com.rblbank.upi", "com.lcode.allahabadupi", "com.fss.ubipsp", "com.lcode.csbupi",
        "com.lcode.ucoupi", "com.olive.andhra.upi", "com.dena.upi.gui", "com.infra.boiupi",
        "com.fss.jnkpsp", "com.infrasofttech.indianbankupi", "com.infrasoft.uboi",
        "com.fisglobal.bandhanupi.app", "com.dbs.in.digitalbank", "com.mgs.hsbcupi"};


    private String packgName;
    private String payURI;

    private CallbackContext callBckCntx;
    private Context ctx = null;
    private static final boolean IS_AT_LEAST_LOLLIPOP = Build.VERSION.SDK_INT >= 21;

    private static final int PAYMENT_REQ_CODE = 199;

    private StringBuilder strBuildr;


	@Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        callBckCntx = callbackContext;
        ctx = IS_AT_LEAST_LOLLIPOP ? cordova.getActivity().getWindow().getContext() : cordova.getActivity().getApplicationContext();

        if(action.equals("PaymentApps")) {
        	cordova.getThreadPool().execute(new Runnable() {
        		public void run() {
        			try {
                        JSONArray  appList = new JSONArray();
                        PackageManager pm = ctx.getPackageManager();
                        List<PackageInfo> packs = pm.getInstalledPackages(0);

                        for (int i = 0; i < packs.size(); i++) {
                            PackageInfo p = packs.get(i);

                            if ((isSystemPackage(p) == false)) {
                                if (Arrays.asList(upiAppPkgs).contains(p.applicationInfo.packageName)) {
                                    JSONObject info = new JSONObject();
                                    info.put("pkgName",p.applicationInfo.packageName);
                                    info.put("appName",p.applicationInfo.loadLabel(pm).toString());
                                    info.put("appIcon",drawableToBase64(p.applicationInfo.loadIcon(pm)));
                                    appList.put(info);
                                }
                            }
                        }
                        JSONObject apps = new JSONObject();
                        apps.put("apps", appList);
                        callBckCntx.success(apps.toString());

        			}catch(Exception e) {
                        e.printStackTrace();
                        String mssge = e.toString();
        				callBckCntx.error(mssge);
        			}
        		}
        	});

            return true;
        }else if(action.equals("StartApp")){
        	packgName = args.getString(0);
            payURI = args.getString(1);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    try {
                        cordova.setActivityResultCallback(AndroidInstalledApps.this);
                        Intent intent = new Intent();
                        intent.setPackage(packgName);
                        intent.setData(Uri.parse(payURI));
                        cordova.startActivityForResult((CordovaPlugin)AndroidInstalledApps.this, intent, PAYMENT_REQ_CODE);

                        // PluginResult pluginResult = new  PluginResult(PluginResult.Status.NO_RESULT);
                        // pluginResult.setKeepCallback(true);
                    }catch(Exception e) {
                        e.printStackTrace();
                        String mssge = e.toString();
                        callBckCntx.error(mssge);
                    }
                }
            });

        	return true;
        }

        return false;
    }

    /**
     * Check for System app or not
     * */
    private boolean isSystemPackage(PackageInfo pkgInfo) {
        return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true : false;
    }


    public String drawableToBase64(Drawable drawable) {
        Bitmap bitmap= getBitmap(drawable);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        String encodedImage = Base64.encodeToString(bitmapdata, Base64.DEFAULT);

        return encodedImage;
    }


    public Bitmap getBitmap(Drawable drawable) {

        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable) drawable).getBitmap();

        } else if (drawable instanceof AdaptiveIconDrawable) {
            Drawable backgroundDr = ((AdaptiveIconDrawable) drawable).getBackground();
            Drawable foregroundDr = ((AdaptiveIconDrawable) drawable).getForeground();

            Drawable[] drr = new Drawable[2];
            drr[0] = backgroundDr;
            drr[1] = foregroundDr;

            LayerDrawable layerDrawable = new LayerDrawable(drr);

            int width = layerDrawable.getIntrinsicWidth();
            int height = layerDrawable.getIntrinsicHeight();

            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bitmap);

            layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            layerDrawable.draw(canvas);

        }

        return bitmap;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        strBuildr = new StringBuilder();

        if (resultCode == -1 && requestCode == PAYMENT_REQ_CODE) {
          if(data != null){
              Bundle bundle = data.getExtras();
              if (bundle != null){
                  Set<String> keys = bundle.keySet();
                  Iterator<String> it = keys.iterator();

                  strBuildr.append("{");

                  while (it.hasNext()) {
                      String key = it.next();
                      String val = "\"" + key + "\":\"" + bundle.get(key)+"\"";
                      strBuildr.append(val);
                      strBuildr.append(",");
                  }

                  strBuildr.append("}");
              }

              if (strBuildr.toString().contains("SUCCESS") || strBuildr.toString().contains("success")){
                callBckCntx.success("SUCCESS");
              }else if (strBuildr.toString().contains("failed") || strBuildr.toString().contains("FAILED")
                      || strBuildr.toString().contains("failure") || strBuildr.toString().contains("FAILURE")
                      || strBuildr.toString().contains("fail") || strBuildr.toString().contains("FAIL")){
                callBckCntx.success("FAILURE");
              }

              callBckCntx.success(strBuildr.toString());

          }else{
              Log.d("TAG","Payment Canceled" );
              callBckCntx.error("CANCELED");
          }
        }

    }

}
