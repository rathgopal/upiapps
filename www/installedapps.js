var installedapps = {

    getAllPaymentApp: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "AndroidInstalledApps", "PaymentApps", []); 
    },

    startPayment: function(packgName, URI, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "AndroidInstalledApps", "StartApp", [packgName, URI]); 
    }

};
module.exports = installedapps;